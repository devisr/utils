<?php

/**
 * Iterator trait based off of the class shown on php.net:
 * http://php.net/manual/en/class.iterator.php
 * 
 * This trait can be included in any class to make it correctly
 * implement the Iterator interface
 */


namespace Devisr\Utils\Arrays;

trait Iterator {
    private $array = [];
    private $position = 0;

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->array[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->array[$this->position]);
    }
}