<?php

namespace Devisr\Utils\Arrays;

use \Devisr\Utils\StringObject;

/**
 * Array management, object oriented style.
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
class ArrayObject implements \ArrayAccess, \Iterator, \Countable {    
    use ArrayAccess;
    use Iterator;

    /**
     * Constructs a new array object
     *
     * @param array $array the initial array to use
     */
    public function __construct(array $array = []) {
        $this->array = $array;
    }

    /**
     * Cast the object to an array
     *
     * @return array the resulting array
     */
    public function toArray(): array {
        return $this->array;
    }

    /**
     * Counts the amount of elements there are in the array
     *
     * @return integer the amount of elements in the array
     */
    public function count(): int {
        return count($this->array);
    }
    
    /**
     * Checks to see if all elements of the array are
     * of a certain primitive type
     *
     * @param string $type the type to check for in the array
     * @return boolean true if all elements of type $type or false if not
     */
    public function isType(string $type): bool {
        return $this->fitsCondition("gettype", $type);
    }

    /**
     * Checks to see if all elements in the array fit a particular 
     * condition.
     *
     * @param callable $function the function to apply to each element
     * @param mixed $equals the value to check against the return value of $function
     * @param array $args an array of arguments to be passed to $function after the array element
     * @return boolean true if all elements fit the particular condition or false if not
     */
    public function fitsCondition(callable $function, $equals, array $args = []): bool {
        foreach($this->array as $element) {
            if($function($element, ...$args) != $equals) return false;
        }

        return true;
    }

    /**
     * Joins together the array elements with a delimiter
     *
     * @param string $delimiter the delimiter to join elements with
     * @return StringObject the resulting string
     */
    public function join(string $delimiter = " "): StringObject {
        return new StringObject(implode($delimiter, $this->array));
    }

    /**
     * Applies a callback function to each element of the array
     *
     * @param callable $function the function to apply to every element of the array
     * @return array the resulting array
     */
    public function map(callable $function): array {
        return $this->array = array_map($function, $this->array);
    }
}