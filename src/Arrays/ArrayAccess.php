<?php

namespace Devisr\Utils\Arrays;

trait ArrayAccess {
    private $array = [];

    /**
     * Set an array element functional style 
     * ie. $this["a"] = "b";
     *
     * @param mixed $offset the array element key to use
     * @param mixed $value the array element value to set
     * @return void
     */
    public function offsetSet($offset, $value) {
        if(is_null($offset)) $this->array[] = $value;
        else $this->array[$offset] = $value;
    }

    /**
     * Get an array element functional style
     * ie. echo $this["a"];
     *
     * @param string|int $offset the array element key to retrieve
     * @return mixed the value of the array element or null
     */
    public function offsetGet($offset) {
        return $this->array[$offset] ?? null;
    }

    /**
     * Unset an array element functional style
     * ie. unset($this["a"])
     *
     * @param string|int $offset the array element key to unset
     * @return void
     */
    public function offsetUnset($offset) {
        unset($this->array[$offset]);
    }

    /**
     * Check if an array element exists functional style
     *
     * @param string|int $offset the array element key to check if exists
     * @return void
     */
    public function offsetExists($offset) {
        return isset($this->array[$offset]);
    }
}