<?php

namespace Devisr\Utils;

use \Devisr\Utils\Arrays\ArrayObject;
use \InvalidArgumentException;

/**
 * String manipulation, object-oriented style.
 * All methods modify the source string
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
class StringObject {
    const EXPRESSION_REGEX = "/^\/.+\/[a-z]*$/i";
    const SENTENCE_REGEX = "/\.([\s]+)([a-z])/";
    const DEFAULT_TRIM_CHARLIST = " \n\t\r\0\x0B";

    private $string;

    /**
     * Constructs a new StringObject
     * 
     * @param string $string the source string to manipulate
     * @return void
     */
    public function __construct(string $string) {
        $this->string = $string;
    }

    /**
     * Converts the $this to a string
     * 
     * @return string the resulting string
     */
    public function __toString(): string {
        return $this->string;
    }

    /**
     * Checks if the source string starts with another string
     * 
     * @param string $start the string to check if $this->string starts with
     * @return boolean true if $this->string starts with $start or false if not
     */
    public function startsWith(string $start): bool {
        return substr($this->string, 0, strlen($start)) == $start;
    }

    /**
     * Checks if the source string ends with another string
     * 
     * @param string $end the string to check if $this->string ends with
     * @return boolean true if $this->string ends with $end or false if not
     */
    public function endsWith(string $end): bool {
        return substr($this->string, -strlen($end)) == $end;
    }

    /**
     * Checks to see if the source string is a regular expression
     *
     * @return boolean true if the source string is a regular expression or false if not
     */
    public function isRegex(): bool {
        return preg_match(self::EXPRESSION_REGEX, $this->string);
    }

    /**
     * Performs a replacement of a string(s).  This can handle regular expressions and regular
     * string replacements.
     *
     * @param string|array $match the string or array of strings to replace
     * @param string|array|callable $replace the string, array of strings or callback to replace $match with
     * @return string
     */
    public function replace($match, $replace, int $limit = -1): string {
        return $this->string = ([ 

            "string" => function() use ($match, $replace, $limit) {
                return (new self($match))->isRegex() ? $this->replaceRegex($match, $replace, $limit) : $this->replaceString($match, $replace);
            },

            "array" => function() use ($match, $replace, $limit) {
                return (new ArrayObject($match))->fitsCondition(function($element) {
                    return preg_match(self::EXPRESSION_REGEX, $element);
                }, true) ? $this->replaceRegex($match, $replace, $limit) : $this->replaceString($match, $replace);
            }

        ][gettype($match)] ?? function() { 
            throw new InvalidArgumentException("\$match must be a string or non empty array");
        })();
    }
    
    /**
     * Performs string replacement with str_replace, does not modify source string
     * (Helper method for ArrayObject::replace)
     *
     * @param string|array $match the string to be replaced
     * @param string|array $replace the string to replace $match with
     * @return string a string where $match has been replaced with $replace
     */
    private function replaceString($match, $replace): string {
        return str_replace($match, $replace, $this->string);
    }

    /**
     * Performs string replacement with preg_replace, does not modify source string
     * (Helper method for ArrayObject::replace)
     *
     * @param string|array $match the string to be replaced
     * @param string|array|callable $replace the string to replace $match with
     * @return string a string where $match has been replaced with $replace
     */
    private function replaceRegex($match, $replace, int $limit = -1): string {
        return is_callable($replace) ? preg_replace_callback($match, $replace, $this->string, $limit) : preg_replace($match, $replace, $this->string, $limit);
    }

    /**
     * Trims excess whitespace from the source string
     *
     * @param string $charlist The list of whitespace characters to remove
     * @return string the string without whitespace characters on both tails
     */
    public function trim(string $charlist = self::DEFAULT_TRIM_CHARLIST): string {
        return $this->string = trim($this->string, $charlist);
    }

    /**
     * Trims excess whitespace from the source string on the left tail
     *
     * @param string $charlist the list of whitespace characters to remove
     * @return string the string without whitespace characters on the left tail
     */
    public function trimStart(string $charlist = self::DEFAULT_TRIM_CHARLIST): string {
        return $this->string = ltrim($this->string, $charlist);
    }

    /**
     * Trims excess whitespace from the source string on the right tail
     *
     * @param string $charlist the list of whitespace characters to remove
     * @return string the string without whitespace characters on the right tail
     */
    public function trimEnd(string $charlist = self::DEFAULT_TRIM_CHARLIST): string {
        return $this->string = rtrim($this->string, $charlist);
    }

    /**
     * Splits the string into multiple parts by a delimiter
     *
     * @param string $delimiter the delimiter used to split the string
     * @return ArrayObject the resulting array of strings
     */
    public function split(string $delimiter = " "): ArrayObject {
        return new ArrayObject(explode($delimiter, $this->string));
    }

    /**
     * Repeats a string
     *
     * @param integer $count the amount of times to repeat the string
     * @return string the resulting string
     */
    public function repeat(int $count): string {
        return $this->string = str_repeat($this->string, $count);
    }

    /**
     * Converts all characters to lowercase
     *
     * @return string the resulting, lowercase string
     */
    public function toLowercase(): string {
        return $this->string = strtolower($this->string);
    }

    /**
     * Converts all characters to uppercase
     *
     * @return string the resulting, uppercase string
     */
    public function toUppercase(): string {
        return $this->string = strtoupper($this->string);
    }

    /**
     * Converts the string to sentence case. Capitalizes the
     * first letter of every sentence.
     *
     * @return string the resulting, sentence-cased string
     */
    public function toSentencecase(): string {
        return $this->string = preg_replace_callback(self::SENTENCE_REGEX, function($matches) {
            return ".".$matches[1].strtoupper($matches[2]);
        }, ucfirst($this->string));
    }
}