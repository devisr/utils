<?php

namespace Devisr\Utils;

use \ReflectionMethod;

/**
 * Easy property management.
 * 
 * @author Cythral <talen.fisher@cythral.com>
 */
trait Properties {
    protected $_props = [];
    
    /**
     * Getter method.  This checks the following places for the property
     * in order:
     * 
     * 1. get{Key}() method in the class
     * 2. key in the _props array
     * 3. ___get() method in the class
     * 
     * @param string $key the property to get
     * @return mixed this will return the value of the property or null if nothing found
     */
    public function __get(string $key) {
        if(method_exists($this, "get{$key}") && (new ReflectionMethod($this, "get{$key}"))->isProtected()) {
            return $this->{"get{$key}"}();
        }

        return $this->_props[strtolower($key)] ?? 
            ((method_exists($this, "___get")) ? $this->___get($key) : null);
    }

    /**
     * Setter method. This first checks for a set{Key}() method
     * If that exists, it will be run.  If that method returns void, nothing else will happen.
     * If a value is returned or the set{Key}() method doesn't exist, it will proceed to execute the following in order:
     * 
     * 1. ___set() method in the class if it exists
     * 2. key in the _props array
     * 
     * @param string $key the name of the property to set
     * @param mixed $val the value to set the property to
     * @return void
     */
    public function __set(string $key, $val) {
        if(method_exists($this, "set{$key}") && (new ReflectionMethod($this, "set{$key}"))->isProtected()) {
            $val = $this->{"set{$key}"}($val);
            if(!$val) return;
        }

        if(method_exists($this, "___set")) $this->___set($key, $val);
        $this->_props[strtolower($key)] = $val;
    }

    /**
     * Checks to see if a property has been set
     * 
     * @param string $key the property to check for
     * @return bool true if the property exists, false if not
     */
    public function __isset(string $key): bool {
        return (method_exists($this, "get{$key}") && (new ReflectionMethod($this, "get{$key}"))->isProtected()) ||
            isset($this->_props[strtolower($key)]) ||
            (method_exists($this, "___get") && $this->___get($key) != null);
    }
}