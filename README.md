# Devisr Utils
This package is a collection of small utilities for creating PHP applications.  Currently, Devisr utils includes the following:

- Properties: a trait that makes handling property encapsulation easy.
- Arrays:
  -  ArrayObject: object oriented array management
  -  Iterator: a trait that can be added to a class to make it Iterable.  
  -  ArrayAccess: a trait that can be added to a class to implement the ArrayAccess interface
- StringObject: object oriented string management
- IP: check if an IP is within a subnet (both IPv4 and IPv6)