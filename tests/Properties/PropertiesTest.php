<?php

include "TestClass.php";

use PHPUnit\Framework\TestCase;

class PropertiesTest extends TestCase {

    public function testArrayGet() {
        $class = new class extends TestClass {
            public function __construct() {
                $this->_props["a"] = "b";
            }
        };

        $this->assertEquals("b", $class->a);
    }

    public function testMethodGet() {
        $class = new class extends TestClass {
            protected function getA() {
                return "b";
            }
        };

        $this->assertEquals("b", $class->a);
    }

    public function testOverrideGet() {
        $class = new class extends TestClass {
            public function ___get($key) {
                if($key == "a") return "b";
            }
        };

        $this->assertEquals("b", $class->a);
    }

    public function testArraySet() {
        $class = new class extends TestClass {};
        $class->a = "b";

        $this->assertEquals("b", $class->a);
    }

    public function testMethodCascadeSet() {
        $class = new class extends TestClass {
            protected function setA($val) {
                return $val."c";
            }
        };
        $class->a = "b";

        $this->assertEquals("bc", $class->a);
    }

    public function testMethodReturnSet() {
        $class = new class extends TestClass {
            protected function setA($val) {}
        };
        $class->a = "b";

        $this->assertEquals(null, $class->a);
    }

    public function testOverrideSet() {
        $class = new class extends TestClass {
            public function ___set($key, $val) {
                if($key == "a") $this->b = $val;
            }
        };
        $class->a = "b";

        $this->assertEquals("b", $class->a);
        $this->assertEquals("b", $class->b);
    }
}