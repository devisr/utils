<?php

use \Devisr\Utils\IP;
use \PHPUnit\Framework\TestCase;

class IPTest extends TestCase {

    /**
     * @dataProvider providerInRange
     */
    public function testInRange($testip, $range) {
        $ip = new IP($testip);
        $this->assertTrue($ip->inRange($range));
    }

    public function providerInRange() {
        return [
            [ "10.8.0.1", "10.8.0.0/24" ],
            [ "2001:db8:abcd::1", "2001:db8:abcd::/64" ]
        ];
    }

    /**
     * @dataProvider providerInRangeFalse
     */
    public function testInRangeFalse($testip, $range) {
        $ip = new IP($testip);
        $this->assertFalse($ip->inRange($range));
    }

    public function providerInRangeFalse() {
        return [
            [ "10.8.0.32", "10.8.0.0/27" ],
            [ "2001:db8:abcf::1", "2001:db8:abcd::/64" ],
            [ "8.8.8.8", "2001:db8:abcd::/64" ]
        ];
    }
}