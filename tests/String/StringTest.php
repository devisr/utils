<?php

use \Devisr\Utils\StringObject;
use PHPUnit\Framework\TestCase;

class StringTest extends TestCase {
    public function testStartsWithGood() {
        $string = new StringObject("abcdefg");
        $this->assertTrue($string->startsWith("abc"));
    }

    public function testStartsWithBad() {
        $string = new StringObject("abcdefg");
        $this->assertFalse($string->startsWith("def"));
    }

    public function testEndsWithGood() {
        $string = new StringObject("abcdefg");
        $this->assertTrue($string->endsWith("efg"));
    }

    public function testEndsWithBad() {
        $string = new StringObject("abcdefg");
        $this->assertFalse($string->endsWith("abc"));
    }

    public function testTrim() {
        $string = new StringObject("  abcdefg  ");
        $this->assertEquals("abcdefg", $string->trim());
    }

    public function testTrimStart() {
        $string = new StringObject("  abcdefg  ");
        $this->assertEquals("abcdefg  ", $string->trimStart());
    }

    public function testTrimEnd() {
        $string = new StringObject("  abcdefg  ");
        $this->assertEquals("  abcdefg", $string->trimEnd());
    }

    public function testSplit() {
        $string = new StringObject("a b c");
        $this->assertEquals(["a", "b", "c"], $string->split()->toArray());
    }

    public function testToSentencecase() {
        $string = new StringObject("hi there. my name is bob");
        $this->assertEquals("Hi there. My name is bob", $string->toSentencecase());
    }
}