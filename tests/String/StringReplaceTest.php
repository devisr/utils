<?php

use \Devisr\Utils\StringObject;
use PHPUnit\Framework\TestCase;

class StringReplaceTest extends TestCase {
    public function testReplaceStringMatchStringReplace() {
        $string = new StringObject("abcdefg");
        $this->assertEquals("abcdefh", $string->replace("g", "h"));
    }

    public function testReplaceArrayMatchStringReplace() {
        $string = new StringObject("abcdefg");
        $this->assertEquals("abcdehh", $string->replace(["f", "g"], "h"));
    }

    public function testReplaceArrayMatchArrayReplace() {
        $string = new StringObject("abcdefg");
        $this->assertEquals("abcdehi", $string->replace(["f", "g"], ["h", "i"]));
    }

    public function testReplaceRegexMatch() {
        $string = new StringObject("abcdefg");
        $this->assertEquals("1", $string->replace("/([a-z]+)/is", "1"));
    }

    public function testReplaceArrayRegexMatch() {
        $string = new StringObject("abcdefg12345");
        $this->assertEquals("aa", $string->replace(["/([a-z]+)/is", "/([0-9]+)/is"], "a"));
    }

    public function testReplaceArrayRegexMatchAndReplace() {
        $string = new StringObject("abcdefg12345");
        $this->assertEquals("ab", $string->replace(["/([a-z]+)/is", "/([0-9]+)/is"], ["a", "b"]));
    }

    public function testReplaceRegexCallback() {
        $string = new StringObject("abcdefg");
        $this->assertEquals("1", $string->replace("/([a-z]+)/is", function() {
            return "1";
        }));
    }
}