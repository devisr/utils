<?php

use Devisr\Utils\Arrays\ArrayObject;
use PHPUnit\Framework\TestCase;

class ArrayObjectTest extends TestCase {

    /**
     * @dataProvider providerIsTypeTrue
     */
    public function testIsTypeTrue($array, $type) {
        $test = new ArrayObject($array);
        $this->assertTrue($test->isType($type));
    }

    public function providerIsTypeTrue() {
        return [
            [ [ "a", "b", "c" ], "string" ],
            [ [ 1, 2, 3 ], "integer" ],
            [ [ null ], "NULL" ]
        ];
    }

    /**
     * @dataProvider providerIsTypeFalse
     */
    public function testIsTypeFalse($array, $type) {
        $test = new ArrayObject($array);
        $this->assertFalse($test->isType($type));
    }

    public function providerIsTypeFalse() {
        return [
            [ [ "a", 1, "c" ], "string" ],
            [ [ 1, "b", 3 ], "integer" ],
            [ [ null, "c" ], "NULL"]
        ];
    }

    public function testJoin() {
        $test = new ArrayObject([ "a", "b", "c" ]);
        $this->assertEquals("a b c", $test->join());
    }

    public function testMap() {
        $test = new ArrayObject([ " a ", " b ", " c "]);
        $test->map("trim");
        $this->assertEquals(["a", "b", "c"], $test->toArray());
    }
}